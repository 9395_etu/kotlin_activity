package com.example.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button = findViewById<Button>(R.id.button)
        button.setOnClickListener{
            val str = findViewById<EditText>(R.id.Age).text.toString()
            val age = try { str.toInt() } catch (e: NumberFormatException) {0}
            if (age > 0)
            {val intent = Intent(this, InfoActivity::class.java)
                intent.putExtra("Age", age)
                startActivity(intent)}
            else
            {Toast.makeText(applicationContext, "Некорректное значение возраста", LENGTH_SHORT).show()}
        }

    }
}
