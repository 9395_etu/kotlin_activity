package com.example.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val age = intent.getIntExtra("Age", 0)
        findViewById<TextView>(R.id.AgeText).text = "Your age is $age"
        val mText = findViewById<TextView>(R.id.MainText)
        when(age)
        {
            in 0..1 -> mText.setText(R.string.infant)
            in 2..11 -> mText.setText(R.string.baby)
            in 11..15 -> mText.setText(R.string.pre_teen)
            in 15..18 -> mText.setText(R.string.teen)
            in 19.. 59-> mText.setText(R.string.adult)
            in 60..99 -> mText.setText(R.string.old)
            else -> mText.setText(R.string.are_you_even_alive)

        }
    }
}
